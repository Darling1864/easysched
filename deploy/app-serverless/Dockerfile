FROM node:16.15-alpine AS node

WORKDIR /app/src/schedule/static/schedule/schedule-frontend

COPY src/schedule/static/schedule/schedule-frontend/package.json .
COPY src/schedule/static/schedule/schedule-frontend/package-lock.json .
RUN npm install @vuepic/vue-datepicker && npm install
RUN npm ci

COPY src/schedule/static/schedule/schedule-frontend .
RUN npm run build

WORKDIR /app/src/schedule/static/schedule/lecturer-frontend

COPY src/schedule/static/schedule/lecturer-frontend/package.json .
COPY src/schedule/static/schedule/lecturer-frontend/package-lock.json .
RUN npm install @vuepic/vue-datepicker && npm install
RUN npm ci

COPY src/schedule/static/schedule/lecturer-frontend .
RUN npm run build


FROM python:3.10

ENV PYTHONUNBUFFERED 1

RUN pip install poetry==1.4.2 && poetry config virtualenvs.create false

WORKDIR /app

RUN git init  # for pre commit hook

COPY pyproject.toml .
COPY poetry.lock .
COPY .pre-commit-config.yaml .
RUN pip install pre-commit
RUN poetry install && pre-commit install --install-hooks

COPY . .

COPY --from=node /app/src/schedule/static/schedule/schedule-frontend/dist /app/src/schedule/static/schedule/schedule-frontend/dist
COPY --from=node /app/src/schedule/static/schedule/lecturer-frontend/dist /app/src/schedule/static/schedule/lecturer-frontend/dist

RUN pre-commit run -a && python src/manage.py collectstatic --noinput

ENV PORT 8000

CMD uwsgi --ini deploy/app-serverless/uwsgi.ini --http-socket :$PORT --static-map /static=/app/static