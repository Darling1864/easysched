# 1. Session schedule generator.
The application for automating the generation of a session schedule for ITIS.

## 2. Description
Веб-сервис для генерации расписания экзаменационной и зачетной сессий. Преподаватели и кафедра указывают свои пожелания, и на их основе генерируется расписание сессии, удовлетворяющее правилам составления расписания.




## 3. Run
- Cоздание виртуального окружения:
```shell
  python3 -m venv .venv
```
- Вход в виртуальное окружение:
```shell
  source ./.venv/bin/activate
```
- Установка зависимостей:
```shell
  pip install -r requirements.txt
```
- Запуск сервера:
```shell
  python3 manage.py runserver
```

## 4. Contributors
| Person      | GitLab | Role |
| ----------- | ----------- |---|
| Evstratova Darya | [Darling1864](https://gitlab.com/Darling1864) | Team leader |
| Novak Sergey | [sergeynovak1](https://gitlab.com/sergeynovak1) | Tech leader
| Talova Olesya | [ttalova](https://gitlab.com/ttalova) | Developer |
| Shakurov Amir | [Shakur_222](https://gitlab.com/Shakur_222) | Developer |
| Ivanov Egor | [IEgor83](https://gitlab.com/IEgor83) | Developer |