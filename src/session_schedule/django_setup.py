import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "session_schedule.settings")
django.setup()
