from django.urls import path
from rest_framework.routers import SimpleRouter
from api.views import (
    WishesViewSet,
    LecturersViewSet,
    SubjectViewSet,
    GroupsBySubject,
    LecturersViewSet,
    CompletionViewSet,
    LecturerFormViewSet,
)

router = SimpleRouter()
router.register(r"subjects", SubjectViewSet, basename="subjects")

urlpatterns = [
    path("wishes/<int:id>/", WishesViewSet.as_view()),
    path("wishes/", WishesViewSet.as_view()),
    path("lecturers_form/", LecturerFormViewSet.as_view()),
    path("lecturers_form/<int:id>/", LecturerFormViewSet.as_view()),
    path("lecturers/<int:id>/", LecturersViewSet.as_view()),
    path("lecturers/", LecturersViewSet.as_view()),
    path("groups/<str:subject_name>/", GroupsBySubject.as_view()),
    path("lecturer_add/", LecturersViewSet.as_view()),
    path("lecturer_edit/<int:id>/", LecturersViewSet.as_view()),
    path("completion/<int:pk>/", CompletionViewSet.as_view(), name="completion"),
] + router.urls
