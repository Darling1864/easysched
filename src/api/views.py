import json

from django.http import JsonResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime
from rest_framework.viewsets import ModelViewSet

from api.serializers import (
    WishesSerializer,
    GroupSerializer,
    SubjectSerializer,
    LecturerSerializer,
    ConnectionSerializer,
    CompletionSerializer,
    ConnectionSaveSerializer,
)
from schedule.models import (
    Wishes,
    Lecturer,
    Subject,
    Group,
    SubjectLectorGroup,
    Completion,
)


class WishesViewSet(APIView):
    """
    Обработка get и post запросов к API
    возвращает все пожелания преподавателей и их время проведения экзаменов
    """

    def get(self, request, *args, **kwargs):
        if kwargs.get("id", None) is not None:
            lst = Wishes.objects.filter(lecturer=kwargs["id"]).values()
        else:
            lst = Wishes.objects.all().values()
        return Response(lst)

    def post(self, request):
        for data in request.data:
            try:
                lecturer = Lecturer.objects.get(id=data["lecturer"])
                Wishes.objects.filter(lecturer=lecturer.id).delete()
            except Lecturer.DoesNotExist:
                return JsonResponse({"msg": "e-mail is not found"}, status=400)

        serializer = WishesSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
        else:
            return JsonResponse({"msg": serializer.errors}, status=400)
        return JsonResponse({"msg": "successfully!"}, status=201)


class LecturerFormViewSet(APIView):
    def get(self, request, *args, **kwargs):
        if kwargs.get("id", None) is not None:
            lst = Lecturer.objects.get(id=kwargs["id"])
            serializer = LecturerSerializer(lst)
        else:
            lst = Lecturer.objects.all().values()
            serializer = LecturerSerializer(lst, many=True)
        return Response(serializer.data)


class LecturersViewSet(APIView):
    def get(self, request, *args, **kwargs):
        connection = dict()
        data = dict()
        if kwargs.get("id", None) is not None:
            lst = Lecturer.objects.get(id=kwargs["id"])
            serializer = LecturerSerializer(lst)
            con = SubjectLectorGroup.objects.filter(lecture=lst)
            for i in con:
                ser = ConnectionSerializer(i)
                sub_name = ser.data["subject"]["subject_name"]
                gr = ser.data["group"]["group_number"]
                connection.setdefault(sub_name, []).append(gr)
            data = serializer.data.copy()
        data["connection"] = connection
        return Response(data)

    def post(self, request, *args, **kwargs):
        print(request.data)
        data_of_lecturer = request.data[0]
        data_of_connection = request.data[1]
        try:
            if kwargs.get("id", None) is not None:
                lecturer = Lecturer.objects.get(id=kwargs["id"])
                serializer = LecturerSerializer(lecturer, data=data_of_lecturer)
            else:
                serializer = LecturerSerializer(data=data_of_lecturer)
            if serializer.is_valid():
                serializer.save()
            else:
                return JsonResponse({"err": serializer.errors}, status=400)
            lecturer = Lecturer.objects.get(email=data_of_lecturer["email"])
            for i in data_of_connection:
                sub = list(data_of_connection[i]["data_connection"].keys())[0]
                print(sub)
                groups = data_of_connection[i]["data_connection"][sub]
                print(groups)
                for j in groups:
                    subject = Subject.objects.get(subject_name=sub).id
                    group = Group.objects.get(group_number=j).id
                    data = {"subject": subject, "lecture": lecturer.id, "group": group}
                    SubjectLectorGroup.objects.filter(lecture=lecturer).delete()
                    serializer = ConnectionSaveSerializer(data=data)
                    if serializer.is_valid():
                        serializer.save()
                    else:
                        print(serializer.errors)
                        return JsonResponse({"err": serializer.errors}, status=400)
        except KeyError:
            serializer = LecturerSerializer(data=data_of_lecturer)
            if serializer.is_valid():
                serializer.save()
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse({"msg": "successfully!"}, status=201)


class GroupsBySubject(APIView):
    def get(self, request, subject_name):
        course = Subject.objects.get(subject_name=subject_name).course_number
        date = datetime.now()
        if 12 >= date.month >= 9:
            year = date.year - course + 1
        else:
            year = date.year - course
        groups = Group.objects.filter(year_of_study=year).values()
        groups_list = list(groups)
        return Response(groups_list)


class SubjectViewSet(ModelViewSet):
    def get_serializer_class(self):
        return SubjectSerializer

    def get_queryset(self):
        return Subject.objects.all()


class CompletionViewSet(APIView):
    def put(self, request, *args, **kwargs):
        lecturer_id = kwargs.get("pk")
        try:
            completion = Completion.objects.get(lecturer_id=lecturer_id)
        except Completion.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = CompletionSerializer(completion, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
