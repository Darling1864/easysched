from rest_framework import serializers

from schedule.models import (
    Lecturer,
    Wishes,
    Subject,
    Group,
    SubjectLectorGroup,
    Completion,
)


class LecturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lecturer
        fields = [
            "id",
            "email",
            "last_name",
            "first_name",
            "middle_name",
            "tg",
            "phone_number",
        ]

    def update(self, instance, validated_data):
        instance.email = validated_data.get("email", instance.email)
        instance.last_name = validated_data.get("last_name", instance.last_name)
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.middle_name = validated_data.get("middle_name", instance.middle_name)
        instance.tg = validated_data.get("tg", instance.tg)
        instance.phone_number = validated_data.get(
            "phone_number", instance.phone_number
        )
        instance.save()
        return instance


class WishesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wishes
        fields = [
            "start_time",
            "exam_duration",
            "number_of_exams",
            "wishes",
            "lecturer",
        ]


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ["subject_name"]


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ["group_number"]


class ConnectionSerializer(serializers.ModelSerializer):
    subject = SubjectSerializer()
    group = GroupSerializer()

    class Meta:
        model = SubjectLectorGroup
        fields = ["lecture", "subject", "group"]


class ConnectionSaveSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubjectLectorGroup
        fields = ["lecture", "subject", "group"]


class CompletionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Completion
        fields = ["lecturer", "is_fill"]
