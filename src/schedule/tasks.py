from django.core.mail import send_mail

from schedule.generation_algorithm.main_generation import all_steps_for_generation
from session_schedule.celery import app


@app.task
def celery_all_steps_for_generation(start, end):
    all_steps_for_generation(start, end)


@app.task
def send_mails_celery(
    type_message,
    text_message,
    own_mail,
    valid_lecturers,
):
    send_mail(
        type_message,
        text_message,
        own_mail,
        valid_lecturers,
    )
