from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout

from schedule.forms import AuthForm
from schedule.models import (
    Institute,
    User,
    Group,
    Lecturer,
    Subject,
    SubjectLectorGroup,
    Wishes,
)


def main(request):
    return redirect("instruction")


def login_view(request):
    form = AuthForm()
    message = None
    if request.method == "POST":
        form = AuthForm(request.POST)
        if form.is_valid():
            user = authenticate(request, **form.cleaned_data)
            if user is None:
                message = "Электронная почта или пароль неправильные"
            else:
                request.session["user"] = form.cleaned_data.get("email", "")
                request.session.setdefault(
                    request.session["user"],
                    {"valid_lecturers": [], "sended_letters": False},
                )
                request.session.save()
                login(request, user)
                next_url = "main"
                if "next" in request.GET:
                    next_url = request.GET.get("next")
                return redirect(next_url)
    return render(
        request,
        "schedule/login.html",
        {"form": form, "message": message},
    )


def logout_view(request):
    user_about = request.session.get("user", "")
    your_data = request.session.get(user_about, None)
    current_expiry = request.session.get("_session_expiry")
    logout(request)
    if your_data:
        request.session[user_about] = your_data
        if current_expiry:
            request.session["_session_expiry"] = current_expiry
    return redirect("main")
