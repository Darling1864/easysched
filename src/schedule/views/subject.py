from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from schedule.models import Subject

from schedule.forms import SubjectForm, SubjectDetailForm


@method_decorator(login_required, name="dispatch")
class SubjectsListView(ListView):
    def _render(self, request, subjects):
        return render(
            request,
            "schedule/subjects/subjects_list.html",
            {"subjects": subjects, "title": "Список дисциплин"},
        )

    def get(self, request, *args, **kwargs):
        subjects = Subject.objects.all().order_by("subject_name")
        course_filter = request.GET.get("course")

        if course_filter:
            subjects = subjects.filter(course_number=course_filter)

        if request.GET.get("search"):
            search = request.GET.get("search")
            subjects = subjects.filter(Q(subject_name__icontains=search))
        subjects = subjects.order_by("subject_name")

        return self._render(request, subjects)

    def post(self, request, *args, **kwargs):
        subjects = Subject.objects.all().distinct("subject_name")
        return self._render(request, subjects)


@method_decorator(login_required, name="dispatch")
class SubjectDetailView(DetailView):
    template_name = "schedule/subjects/subject.html"
    slug_field = "subject_name"
    slug_url_kwarg = "subject_name"

    def get_queryset(self):
        return Subject.objects.all()

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "subject_form": SubjectDetailForm(
                initial={"subject_name": self.object.subject_name}
            ),
            "title": self.object.subject_name,
        }


class SubjectMixin:
    template_name = "schedule/subjects/subject_add.html"
    slug_field = "subject_name"
    slug_url_kwarg = "subject_name"

    def get_queryset(self):
        return Subject.objects.all()

    def get_success_url(self):
        return reverse("subject", args=[self.object.subject_name])


@method_decorator(login_required, name="dispatch")
class SubjectCreateFormView(SubjectMixin, CreateView):
    form_class = SubjectForm

    def get_context_data(self, **kwargs):
        return {
            **super(SubjectCreateFormView, self).get_context_data(**kwargs),
            "title": "Добавление дисциплины",
        }


@method_decorator(login_required, name="dispatch")
class SubjectUpdateView(SubjectMixin, UpdateView):
    form_class = SubjectForm

    def get_context_data(self, **kwargs):
        return {
            **super(SubjectUpdateView, self).get_context_data(**kwargs),
            "subject_name": self.kwargs[self.slug_url_kwarg],
            "course_number": self.object.course_number,
            "title": "Редактирование дисциплины",
        }


@method_decorator(login_required, name="dispatch")
class SubjectDeleteView(SubjectMixin, DeleteView):
    template_name = "schedule/subjects/subject_delete.html"

    def get_context_data(self, **kwargs):
        return {
            **super(SubjectDeleteView, self).get_context_data(**kwargs),
            "title": "Удаление дисциплины",
        }

    def get_success_url(self):
        return reverse("subjects")
