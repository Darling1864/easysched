from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from schedule.models import Lecturer, Subject, Group, Wishes, SubjectLectorGroup
from schedule.forms import LecturerForm, ConnectionForm, WishChange


@method_decorator(login_required, name="dispatch")
class LecturersListView(ListView):
    model = Lecturer
    template_name = "schedule/lecturers/lecturers_list.html"
    context_object_name = "lecturers"
    extra_context = {"title": "Список преподавателей"}

    def get_queryset(self):
        search = self.request.GET.get("search")
        lecturers = super().get_queryset().order_by("last_name")
        if search:
            lecturers = lecturers.filter(
                Q(first_name__icontains=search)
                | Q(middle_name__icontains=search)
                | Q(last_name__icontains=search)
            )
        return lecturers

    def post(self, request, *args, **kwargs):
        lecturers = Lecturer.objects.order_by("first_name")
        return self._render(request, lecturers)


@method_decorator(login_required, name="dispatch")
class LecturerDetailView(DetailView):
    template_name = "schedule/lecturers/lecturer_profile.html"
    slug_field = "id"
    slug_url_kwarg = "id"

    def get_queryset(self):
        return Lecturer.objects.all()

    def get_context_data(self, **kwargs):
        lecturer = get_object_or_404(Lecturer, id=self.object.id)
        subjects_and_groups = []
        subjects_groups = SubjectLectorGroup.objects.filter(lecture=self.object.id)
        for sub_group in subjects_groups:
            subjects_and_groups.append(f"{sub_group.subject}: {sub_group.group}")
        title = (
            f"{lecturer.last_name} {lecturer.first_name[0]}. {lecturer.middle_name[0]}."
        )
        return {
            **super().get_context_data(**kwargs),
            "lecturer": lecturer,
            "subjects_and_groups": subjects_and_groups,
            "title": title,
        }


@login_required
def lecturer_form(request, id):
    lecturers = Lecturer.objects.filter(id=id).prefetch_related("wishes_set")

    for lecturer in lecturers:
        wishes = lecturer.wishes_set.all()

    lecturer = Lecturer.objects.get(id=id)
    title = f"Пожелание {lecturer.last_name} {lecturer.first_name[0]}. {lecturer.middle_name[0]}."

    return render(
        request,
        "schedule/lecturers/lecturer_form.html",
        {
            "lecturer": lecturer,
            "wishes": wishes,
            "title": title,
        },
    )


class LecturerMixin:
    template_name = "schedule/lecturers/lecturer_add.html"
    slug_field = "id"
    slug_url_kwarg = "id"

    def get_queryset(self):
        return Lecturer.objects.all()

    def get_success_url(self):
        return reverse("profile", args=[self.object.id])


@method_decorator(login_required, name="dispatch")
class LecturerCreateView(LecturerMixin, CreateView):
    def _render(self, request):
        return render(
            request,
            "schedule/lecturers/lecturer_add.html",
        )

    def get(self, request, *args, **kwargs):
        return self._render(request)


@method_decorator(login_required, name="dispatch")
class LecturerUpdateView(LecturerMixin, UpdateView):
    form_class = LecturerForm

    def get_context_data(self, **kwargs):
        return {
            **super(LecturerUpdateView, self).get_context_data(**kwargs),
            "email": self.object.email,
            # "first_name": self.object.first_name,
            # "last_name": self.object.last_name,
            # "middle_name": self.object.middle_name,
            # "title": "Редактирование профиля",
        }


@method_decorator(login_required, name="dispatch")
class LecturerDeleteView(LecturerMixin, DeleteView):
    template_name = "schedule/lecturers/lecturer_delete.html"

    def get_success_url(self):
        return reverse("lecturers")

    def get_context_data(self, **kwargs):
        return {
            **super(LecturerDeleteView, self).get_context_data(**kwargs),
            "title": "Удаление учителя",
        }
