from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)

from schedule.models import (
    Subject,
    Group,
)
from schedule.forms import GroupForm, GroupDetailForm
from schedule.views.services import calculate_course


@method_decorator(login_required, name="dispatch")
class GroupsListView(ListView):
    def _render(self, request, groups):
        return render(
            request,
            "schedule/groups/groups_list.html",
            {"groups": groups, "title": "Список групп"},
        )

    def get(self, request, *args, **kwargs):
        groups = Group.objects.distinct("group_number")
        if request.GET.get("search") is not None:
            search = request.GET.get("search", None)
            groups = Group.objects.filter(Q(group_number__icontains=search))
        return self._render(request, groups)

    def post(self, request, *args, **kwargs):
        groups = Subject.objects.all().distinct("group_number")
        return self._render(request, groups)


@method_decorator(login_required, name="dispatch")
class GroupDetailView(DetailView):
    template_name = "schedule/groups/group.html"
    slug_field = "group_number"
    slug_url_kwarg = "group_number"

    def get_queryset(self):
        return Group.objects.all()

    def get_context_data(self, **kwargs):
        course = calculate_course(self.object.year_of_study)
        return {
            **super().get_context_data(**kwargs),
            "group_form": GroupDetailForm(
                initial={"group_number": self.object.group_number}
            ),
            "title": self.object.group_number,
            "course": course,
        }


class GroupMixin:
    template_name = "schedule/groups/group_add.html"
    slug_field = "group_number"
    slug_url_kwarg = "group_number"
    model = Group

    def get_queryset(self):
        return self.model.objects.all()

    def get_success_url(self):
        return reverse("group", args=[self.object.group_number])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Добавление группы"
        return context


@method_decorator(login_required, name="dispatch")
class GroupCreateFormView(GroupMixin, CreateView):
    form_class = GroupForm


@method_decorator(login_required, name="dispatch")
class GroupUpdateView(GroupMixin, UpdateView):
    form_class = GroupForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["group_number"] = self.kwargs[self.slug_url_kwarg]
        context["code_institute"] = self.object.code_institute
        context["year_of_study"] = self.object.year_of_study
        context["leader_tg"] = self.object.leader_tg
        context["title"] = "Редактирование группы"
        return context


@method_decorator(login_required, name="dispatch")
class GroupDeleteView(GroupMixin, DeleteView):
    template_name = "schedule/groups/group_delete.html"

    def get_success_url(self):
        return reverse("groups")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Удаление группы"
        return context
