from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.db.models import Q
from django.shortcuts import render

from schedule.models import (
    Lecturer,
    Wishes,
    Completion,
    Timetable,
)

from schedule.tasks import celery_all_steps_for_generation, send_mails_celery


def form(request):
    return render(
        request,
        "schedule/mailing_form.html",
        {"lecturer_email": "", "lecturer_full_name": ""},
    )


def form_edit(request, id):
    lecturer = Lecturer.objects.get(id=id)
    full_name = (
        lecturer.middle_name + " " + lecturer.first_name + " " + lecturer.last_name
    )
    return render(
        request,
        "schedule/mailing_form.html",
        {"lecturer_email": lecturer.email, "lecturer_full_name": full_name},
    )


@login_required
def schedule_generation_view(request):
    ids = list(
        Completion.objects.filter(is_fill=False).values_list("lecturer_id", flat=True)
    )
    tumetables = Timetable.objects.all()
    lecturers = []
    error_message = None
    title = "Генерация расписания"
    for lecturer in Lecturer.objects.all():
        if lecturer.id in ids:
            lecturers.append(lecturer)
    wishes = [wish.lecturer_id for wish in Wishes.objects.all()]
    if request.method == "POST":
        start = [int(i) for i in request.POST.get("start").split("-")]
        end = [int(i) for i in request.POST.get("end").split("-")]
        celery_all_steps_for_generation.delay(start, end)

    return render(
        request,
        "schedule/generation_schedule/schedule_generation.html",
        {
            "not_wishes": ids,
            "lecturers": lecturers,
            "wishes": wishes,
            "title": title,
            "error_message": error_message,
            "tumetables": tumetables,
        },
    )


@login_required
def instruction_view(request):
    title = "Создать расписание"
    ids = list(
        Completion.objects.filter(is_fill=False).values_list("lecturer_id", flat=True)
    )
    lecturers = Lecturer.objects.filter(id__in=ids)
    wishes = [wish.lecturer_id for wish in Wishes.objects.all()]
    all = True
    for lecturer in lecturers:
        if lecturer.id not in wishes:
            all = False
    return render(
        request,
        "schedule/generation_schedule/instruction_list.html",
        {"emails": ids, "all": all, "title": title},
    )


@login_required
def send_letters(request):
    search = request.GET.get("search", None)
    lecturers = Lecturer.objects.all()
    if search:
        lecturers = lecturers.filter(
            Q(first_name__icontains=search)
            | Q(middle_name__icontains=search)
            | Q(last_name__icontains=search)
        )
    message = ""
    if request.method == "POST":
        type_message = request.POST.get("email_type")
        text_message = request.POST.get("email_text")
        try:
            valid_lecturers = dict(request.POST)["list"]
            send_mails_celery.delay(
                type_message,
                text_message,
                "session.schedule.kfu@yandex.ru",
                valid_lecturers,
            )
            for email in valid_lecturers:
                lecturer_id = Lecturer.objects.get(email=email).id
                try:
                    Completion.objects.get(lecturer_id=lecturer_id, is_fill=False)
                except:
                    Completion.objects.create(lecturer_id=lecturer_id, is_fill=False)
            message = "Письма успешно отправлены"
        except:
            if "list" in dict(request.POST):
                message = (
                    "Письма не были отправлены, попробуйте позже! Возможно, ваше письмо выглядит "
                    "подозрительным, измените текст!"
                )
            else:
                message = (
                    "Выберите учителей, которым вы хотите отправить сообщение"
                )
    data = {
        "lecturers": lecturers,
        "email_type": "Форма для заполнения пожеланий по проведению экзаменов",
        "text_email": f"Здравствуйте,\n\nМы рады сообщить Вам о том, что мы работаем над улучшением расписания экзаменов для наших студентов. Для этого мы хотели бы узнать Ваше мнение и пожелания относительно времени и даты проведения экзаменов.\nПросим Вас заполнить форму по ссылке ниже, чтобы мы могли учесть Ваши предпочтения при составлении расписания:\n{settings.HOST}/form/\nМы гарантируем конфиденциальность Ваших данных и использование их только в целях составления расписания экзаменов.\nБлагодарим Вас за участие в нашем проекте.\nС уважением,\nИнститут ИТИС",
        "message": message,
        "already_send_lecturers": [
            Lecturer.objects.get(id=id)
            for id in list(Completion.objects.values_list("lecturer_id", flat=True))
        ],
        "search": search,
        "title": "Рассылка форм",
    }
    return render(
        request,
        "schedule/generation_schedule/mailing_to_lecturers.html",
        context=data,
    )


@login_required
def list_wishes_view(request):
    title = "Данные учителей"
    search = request.GET.get("search", None)
    ids = Completion.objects.values_list("lecturer_id", flat=True)
    lecturers = Lecturer.objects.filter(id__in=ids)
    if search:
        lecturers = lecturers.filter(
            Q(first_name__icontains=search)
            | Q(middle_name__icontains=search)
            | Q(last_name__icontains=search)
        )
    wishes = [wish.lecturer_id for wish in Wishes.objects.all()]
    all = True
    for lecturer in lecturers:
        if lecturer.id not in wishes:
            all = False
    if len(wishes) == 0:
        all = False
    return render(
        request,
        "schedule/wishes/wishes_list.html",
        {"lecturers": lecturers, "wishes": wishes, "all": all, "title": title},
    )
