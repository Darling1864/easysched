from datetime import datetime


def calculate_course(year_of_study):
    date = datetime.now()
    if 12 >= date.month >= 9:
        course = date.year - year_of_study + 1
    else:
        course = date.year - year_of_study
    return course
