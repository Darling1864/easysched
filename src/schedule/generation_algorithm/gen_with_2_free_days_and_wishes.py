import random

from schedule.generation_algorithm.func_representation import (
    get_only_dates_from_wishes,
)
from schedule.generation_algorithm.genetic_base_schedule_without_2_free_days import (
    favorable_conditions,
    copy_dictionary,
    check_free_date_of_lecturer,
)
from schedule.generation_algorithm.get_data import get_lecturer_name


def execute_mutation(
    schedule, group, exam_to_mutate, period, actual_holidays, time_of_lecturers
):
    while True:
        data = random.randint(0, len(schedule[group]) - 1)
        if favorable_conditions(
            schedule, group, period, data, exam_to_mutate.discipline, actual_holidays
        ) and check_free_date_of_lecturer(
            schedule, period, exam_to_mutate.lecturer, data, time_of_lecturers
        ):
            schedule[group][data] = exam_to_mutate
            break


def choose_one_random_group(all_groups):
    first_step = random.choice(list(all_groups.values()))
    while not first_step:
        first_step = random.choice(list(all_groups.values()))
    second_step = random.choice(first_step)
    return second_step


def launch_of_generations(schedule, course_group, period, actual_holidays):
    group = choose_one_random_group(course_group)
    index_exam_to_mutate = random.randint(0, len(schedule[group]) - 1)
    count = 0
    while schedule[group][index_exam_to_mutate] == "" or isinstance(
        schedule[group][index_exam_to_mutate], list
    ):
        count += 1
        index_exam_to_mutate = random.randint(0, len(schedule[group]) - 1)
        if count == 200:
            return False
    exam_to_mutate = schedule[group][index_exam_to_mutate]
    schedule[group][index_exam_to_mutate] = ""
    return [schedule, group, exam_to_mutate, period, actual_holidays]


def mutate_schedule(schedule, course_group, period, actual_holidays, time_of_lecturers):
    first_deleted_exam = launch_of_generations(
        schedule, course_group, period, actual_holidays
    )
    while not first_deleted_exam:
        first_deleted_exam = launch_of_generations(
            schedule, course_group, period, actual_holidays
        )
    execute_mutation(*first_deleted_exam, time_of_lecturers)
    return schedule


def one_generation(
    schedule, course_group, period, actual_holidays, score, time_of_lecturers
):
    list_of_schedules = [schedule]
    for _ in range(20):
        schedule_now = copy_dictionary(schedule)
        schedule_now = mutate_schedule(
            schedule_now, course_group, period, actual_holidays, time_of_lecturers
        )
        list_of_schedules.append(schedule_now)
    return list_of_schedules, score


def curse_is(all_groups, group):
    for pare in all_groups.items():
        if group in pare[1]:
            return pare[0]


def count_bad_score_by_wishes_lecturers(schedule, period, time_of_lecturers):
    score = 0
    for day in range(len(period)):
        for group in list(schedule.values()):
            if not isinstance(group[day], str) and not isinstance(group[day], list):
                if period[day] not in get_only_dates_from_wishes(
                    time_of_lecturers, group[day].lecturer
                ):
                    score += 1
    return score


def count_total_score(schedule, all_subjects, period, time_of_lecturers, all_groups):
    return count_bad_score_by_wishes_lecturers(schedule, period, time_of_lecturers)


def choose_the_best_schedule(
    list_of_schedules, all_subjects, period, time_of_lecturers, last_score, all_groups
):
    the_best_schedule = {last_score: list_of_schedules[0]}
    for schedule in list_of_schedules[1:]:
        score = count_total_score(
            schedule, all_subjects, period, time_of_lecturers, all_groups
        )
        if list(the_best_schedule.keys())[0] > score:
            the_best_schedule = {score: schedule}
    return the_best_schedule


def found_not_right_wishes(schedule, period, time_of_lecturers):
    text = ""
    for day in range(len(period)):
        for group in list(schedule.values()):
            if not isinstance(group[day], str) and not isinstance(group[day], list):
                if period[day] not in get_only_dates_from_wishes(
                    time_of_lecturers, group[day].lecturer
                ):
                    text += f"{get_lecturer_name(group[day].lecturer)} не может {period[day]} \n"
    return text
