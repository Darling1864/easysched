def auditorium_is_free_for_list(schedule, auditorium):
    for exam in schedule:
        if exam.auditorium == auditorium:
            return False
    return True


def auditorium_is_free(schedule, auditorium, day):
    for course in schedule:
        if isinstance(course[day], list):
            return auditorium_is_free_for_list(course[day], auditorium)
        elif course[day]:
            if course[day].auditorium == auditorium:
                return False
    return True


def free_auditorium_that_day(schedule, day, list_of_auditoriums):
    for auditorium in list_of_auditoriums:
        if auditorium_is_free(list(schedule.values()), auditorium, day):
            return auditorium
    return None
