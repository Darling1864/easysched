from schedule.generation_algorithm.check_days import (
    is_lecturer_has_no_exams_that_day,
    free_days,
    is_lecturer_has_two_exams_that_day,
    lecturer_has_free_time_to_this_exam,
)
from schedule.generation_algorithm.check_exams import exist_sub_in_schedule_group
from schedule.generation_algorithm.check_auditorium import free_auditorium_that_day
from schedule.generation_algorithm.dataclass_of_one_day import DayOfSchedule
from schedule.generation_algorithm.func_representation import (
    copy_dictionary,
    sum_lists,
)
from schedule.generation_algorithm.get_data import get_streams


def count_bad_score_by_emptiness(schedule):
    score = 0
    for group in schedule.items():
        count = 0
        for day in group[1]:
            if not isinstance(day, str) and not isinstance(day, list):
                count += 1
        if count < len(sum_lists(list(get_streams(group[0]).values()))):
            score += 1
    return score


def favorable_conditions(schedule, group, period, data, sub, holidays):
    if (
        period[data] not in holidays
        and schedule[group][data] == ""
        and not exist_sub_in_schedule_group(schedule[group], sub)
        and free_days(schedule, group, data)
    ):
        return True
    return False


def check_free_date_of_lecturer(schedule, period, lecturer, data, time_of_lecturers):
    if is_lecturer_has_two_exams_that_day(lecturer, period[data], time_of_lecturers):
        return lecturer_has_free_time_to_this_exam(schedule, data, lecturer)
    else:
        return is_lecturer_has_no_exams_that_day(schedule, data, lecturer)


def generate_for_one_group(
    schedule, group, period, auditoriums, holidays, time_of_lecturers
):
    list_of_descipline = get_streams(group)
    for lecturers_discipline in list_of_descipline.items():
        lecturer = lecturers_discipline[0]
        for subject in lecturers_discipline[1]:
            for day in range(len(schedule[group])):
                is_favorite = favorable_conditions(
                    schedule, group, period, day, subject, holidays
                )
                is_free = check_free_date_of_lecturer(
                    schedule, period, lecturer, day, time_of_lecturers
                )
                if is_favorite and is_free:
                    schedule[group][day] = DayOfSchedule(
                        group=group,
                        lecturer=lecturer,
                        discipline=subject,
                        auditorium=free_auditorium_that_day(schedule, day, auditoriums),
                    )
    return schedule


# TODO большинство функций принимает много парамтеров. Может лучше использовать классы?
def some_schedule_with_lecturers_wishes(
    schedule, groups, datas, auditoriums, holidays, time_of_lecturers
):
    list_of_groups = sum_lists(groups.values())
    schedule_now = copy_dictionary(schedule)
    for group in list_of_groups:
        schedule_now = generate_for_one_group(
            schedule_now, group, datas, auditoriums, holidays, time_of_lecturers
        )

    return schedule_now
