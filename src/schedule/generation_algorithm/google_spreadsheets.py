import os

import httplib2
import googleapiclient.discovery

from oauth2client.service_account import ServiceAccountCredentials

from django.conf import settings

'''CREDENTIALS_FILE = os.path.join(settings.BASE_DIR, "schedule", "creds.json")

credentials = ServiceAccountCredentials.from_json_keyfile_name(
    CREDENTIALS_FILE,
    [
        "https://www.googleapis.com/auth/spreadsheets",
        "https://www.googleapis.com/auth/drive",
    ],
)

httpAuth = credentials.authorize(httplib2.Http())
service = googleapiclient.discovery.build("sheets", "v4", http=httpAuth)'''


# def create_spreadsheet(title):
#     credentials = ServiceAccountCredentials.from_json_keyfile_name(
#         CREDENTIALS_FILE,
#         [
#             "https://www.googleapis.com/auth/spreadsheets",
#             "https://www.googleapis.com/auth/drive",
#         ],
#     )
#
#     http_auth = credentials.authorize(httplib2.Http())
#     service = googleapiclient.discovery.build("sheets", "v4", http=http_auth)
#
#     spreadsheet = (
#         service.spreadsheets()
#         .create(body={"properties": {"title": title}}, fields="spreadsheetId")
#         .execute()
#     )
#
#     spreadsheet_id = spreadsheet["spreadsheetId"]
#     return spreadsheet_id


def generate_sheets(
    season, year_of_session, spreadsheet_id, schedule, groups, period, names_of_days
):
    service.spreadsheets().values().batchUpdate(
        spreadsheetId=spreadsheet_id,
        body={
            "valueInputOption": "USER_ENTERED",
            "data": [
                {
                    "range": "A1",
                    "majorDimension": "ROWS",
                    "values": [
                        [
                            f"Расписание экзаменов на {season} {year_of_session} учебный год."
                        ]
                    ],
                },
                {"range": "A2", "majorDimension": "ROWS", "values": [groups]},
                {"range": "A3", "majorDimension": "COLUMNS", "values": [period]},
                {"range": "B3", "majorDimension": "COLUMNS", "values": [names_of_days]},
                {"range": "C3", "majorDimension": "COLUMNS", "values": schedule},
            ],
        },
    ).execute()


def default_settings_google_sheets(spreadsheet_id, groups):
    service.spreadsheets().batchUpdate(
        spreadsheetId=spreadsheet_id,
        body={
            "requests": [
                {
                    "updateDimensionProperties": {
                        "range": {
                            "sheetId": 0,
                            "dimension": "COLUMNS",
                            "startIndex": 0,
                        },
                        "properties": {"pixelSize": 200},
                        "fields": "pixelSize",
                    }
                },
                {
                    "updateDimensionProperties": {
                        "range": {
                            "sheetId": 0,
                            "dimension": "ROWS",
                            "startIndex": 0,
                        },
                        "properties": {"pixelSize": 100},
                        "fields": "pixelSize",
                    }
                },
            ]
        },
    ).execute()
    text = "Расписание экзаменов на 1 семестр 2022-2023 учебный год."
    merge_cells_and_write_text(
        spreadsheet_id, "A", chr(ord("A") + len(groups) + 1), 1, text
    )


def merge_cells_and_write_text(
    spreadsheet_id, start_column, end_column, row_index, text
):
    merge_range = f"{start_column}{row_index}:{end_column}{row_index}"

    merge_request = {
        "mergeCells": {
            "range": {
                "sheetId": 0,
                "startRowIndex": row_index - 1,
                "endRowIndex": row_index,
                "startColumnIndex": ord(start_column) - 65,
                "endColumnIndex": ord(end_column) - 65 + 1,
            },
            "mergeType": "MERGE_ALL",
        }
    }

    format_request = {
        "repeatCell": {
            "range": {
                "sheetId": 0,
                "startRowIndex": row_index - 1,
                "endRowIndex": row_index,
                "startColumnIndex": ord(start_column) - 65,
                "endColumnIndex": ord(end_column) - 65 + 1,
            },
            "cell": {
                "userEnteredFormat": {
                    "horizontalAlignment": "CENTER",
                    "textFormat": {"bold": True},
                }
            },
            "fields": "userEnteredFormat",
        }
    }

    requests = [merge_request, format_request]
    service.spreadsheets().batchUpdate(
        spreadsheetId=spreadsheet_id, body={"requests": requests}
    ).execute()
