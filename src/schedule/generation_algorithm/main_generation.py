from schedule.generation_algorithm.elective_courses import course_schedule
from schedule.generation_algorithm.func_representation import (
    period_of_session,
    reform_list_to_str,
    sum_lists,
)
from schedule.generation_algorithm.gen_with_2_free_days_and_wishes import (
    count_total_score,
    one_generation,
    choose_the_best_schedule,
    found_not_right_wishes,
)
from schedule.generation_algorithm.genetic_base_schedule_without_2_free_days import (
    some_schedule_with_lecturers_wishes,
    count_bad_score_by_emptiness,
)
from schedule.generation_algorithm.get_data import (
    get_groups,
    get_subjects,
    get_auditoriums,
    get_lecturer_time,
)
from schedule.generation_algorithm.google_spreadsheets import (
    default_settings_google_sheets,
    generate_sheets,
    # create_spreadsheet,
)
from schedule.generation_algorithm.timing import (
    get_time_from_withes,
    got_lecturers_name,
)
from schedule.models import Timetable
from session_schedule.settings import SPREADSHEET_ID


def arrangement_of_elective_courses(
    dict_of_groups,
    dict_of_all_subjects_without_elective_curses,
    dict_of_all_elective_curses,
    dict_of_lecturers_wishes_without_elective_curses,
    dict_of_lecturers_of_elective_curses_wishes,
    datas_of_session,
):
    schedule_start = {}
    for i in list(dict_of_groups.keys()):
        lecturers = list(dict_of_all_subjects_without_elective_curses[f"{i}"].keys())
        cource_lecturers = list(dict_of_all_elective_curses[f"{i}"].keys())
        cources = dict_of_all_elective_curses[f"{i}"]
        groups = dict_of_groups[f"{i}"]
        schedule_start.update(
            course_schedule(
                datas_of_session,
                lecturers,
                cource_lecturers,
                cources,
                groups,
                dict_of_lecturers_of_elective_curses_wishes,
                dict_of_lecturers_wishes_without_elective_curses,
            )
        )
    return schedule_start


def generate_session_schedule(
    schedule_start,
    dict_of_all_subjects_without_elective_curses,
    dict_of_groups,
    datas_of_session,
    list_of_auditoriums,
    holidays,
    dict_of_lecturers_wishes_without_elective_curses,
):
    base_schedule = some_schedule_with_lecturers_wishes(
        schedule_start,
        dict_of_groups,
        datas_of_session,
        list_of_auditoriums,
        holidays,
        dict_of_lecturers_wishes_without_elective_curses,
    )
    actual_score = count_total_score(
        base_schedule,
        dict_of_all_subjects_without_elective_curses,
        datas_of_session,
        dict_of_lecturers_wishes_without_elective_curses,
        dict_of_groups,
    )
    finish = base_schedule
    steps = 500
    for i in range(steps):
        list_of_schedules, actual_score = one_generation(
            finish,
            dict_of_groups,
            datas_of_session,
            holidays,
            actual_score,
            dict_of_lecturers_wishes_without_elective_curses,
        )
        finish = choose_the_best_schedule(
            list_of_schedules,
            dict_of_all_subjects_without_elective_curses,
            datas_of_session,
            dict_of_lecturers_wishes_without_elective_curses,
            actual_score,
            dict_of_groups,
        )
        actual_score = list(finish.keys())[0]
        finish = list(finish.values())[0]
        if actual_score == 0:
            break
    text = found_not_right_wishes(
        finish, datas_of_session, dict_of_lecturers_wishes_without_elective_curses
    )
    exam_not_exist = count_bad_score_by_emptiness(finish)
    if exam_not_exist > 0:
        print(f"не смогли расставить {exam_not_exist} экзаменов")
    return finish


def all_steps_for_generation(start_session, finish_session):
    if start_session[1] == 1:
        season = "1 семестр"
    else:
        season = "2 семестр"
    year_of_session = f"{start_session[0]-1}-{start_session[0]}"
    datas_of_session = (
        period_of_session(start_session, finish_session).strftime("%d.%m.%Y").tolist()
    )
    holidays = [
        x.strftime("%d.%m.%Y")
        for x in period_of_session(start_session, finish_session)
        if x.day_name() == "Sunday"
    ]
    names_of_days_russian = [
        "Понедельник",
        "Вторник",
        "Среда",
        "Четверг",
        "Пятница",
        "Суббота",
        "Воскресенье",
    ]
    names_of_days = [
        names_of_days_russian[x.weekday()]
        for x in period_of_session(start_session, finish_session)
    ]

    dict_of_groups = get_groups()
    dict_of_all_subjects_without_elective_curses = get_subjects(0)
    dict_of_all_elective_curses = get_subjects(1)
    list_of_auditoriums = get_auditoriums()
    dict_of_lecturers_wishes_without_elective_curses = get_lecturer_time(0)
    dict_of_lecturers_of_elective_curses_wishes = get_lecturer_time(1)

    schedule_start = arrangement_of_elective_courses(
        dict_of_groups,
        dict_of_all_subjects_without_elective_curses,
        dict_of_all_elective_curses,
        dict_of_lecturers_wishes_without_elective_curses,
        dict_of_lecturers_of_elective_curses_wishes,
        datas_of_session,
    )

    finish_schedule_before_upgrade_to_sheets = generate_session_schedule(
        schedule_start,
        dict_of_all_subjects_without_elective_curses,
        dict_of_groups,
        datas_of_session,
        list_of_auditoriums,
        holidays,
        dict_of_lecturers_wishes_without_elective_curses,
    )

    get_time_from_withes(
        finish_schedule_before_upgrade_to_sheets,
        dict_of_lecturers_wishes_without_elective_curses,
        dict_of_lecturers_of_elective_curses_wishes,
        datas_of_session,
    )
    got_lecturers_name(finish_schedule_before_upgrade_to_sheets, datas_of_session)
    main_schedule = [
        [
            reform_list_to_str(day) if isinstance(day, list) else str(day)
            for day in group
        ]
        for group in finish_schedule_before_upgrade_to_sheets.values()
    ]

    all_groups = ["дата", "дни недели"] + sum_lists(list(dict_of_groups.values()))
    # spreadsheetId_list = create_spreadsheet("Расписание сессии")
    spreadsheetId_list = SPREADSHEET_ID
    default_settings_google_sheets(spreadsheetId_list, all_groups)
    generate_sheets(
        season,
        year_of_session,
        spreadsheetId_list,
        main_schedule,
        all_groups,
        datas_of_session,
        names_of_days,
    )
    Timetable.objects.create(
        exam_year=year_of_session,
        exam_season=season,
        link=f"https://docs.google.com/spreadsheets/d/{spreadsheetId_list}",
    )
