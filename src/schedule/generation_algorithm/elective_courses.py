import datetime as dt
import numpy as np

from schedule.generation_algorithm.dataclass_of_one_day import DayOfSchedule
from schedule.generation_algorithm.func_representation import (
    get_only_dates_from_wishes,
)
from schedule.generation_algorithm.get_data import get_lecturer_name


def get_5_dates_around_day(date):
    return [
        (dt.datetime.strptime(date, "%d.%m.%Y") + dt.timedelta(days=i))
        .date()
        .strftime("%d.%m.%Y")
        for i in range(-2, 3)
    ]


def sort_l_datas(lecturers, time_of_lecturers):
    all_dates = []
    for lecturer in lecturers:
        all_dates += get_only_dates_from_wishes(time_of_lecturers, lecturer)
    return sorted([(i, all_dates.count(i)) for i in set(all_dates)], key=lambda t: t[1])


def sort_c_datas(cource_lecturers, l_frequent_date, courses, time_of_cources):
    all_dates = []
    for lecturer in cource_lecturers:
        if lecturer not in courses:
            all_dates += get_only_dates_from_wishes(time_of_cources, lecturer)
    c_frequent_date = []
    for i in set(all_dates):
        y = []
        for j in get_5_dates_around_day(i):
            y += [item[1] for item in l_frequent_date if item[0] == j]
        c_frequent_date.append((i, all_dates.count(i), sum(y)))
    return sorted(c_frequent_date, key=lambda t: t[2] - t[1])


def course_schedule(
    dates,
    lecturers,
    cource_lecturers,
    subjects,
    groups,
    time_of_cources,
    time_of_lecturers,
):
    courses = dict()
    while len(courses) != len(cource_lecturers):
        l_frequent_date = sort_l_datas(lecturers, time_of_lecturers)
        c_frequent_date = sort_c_datas(
            cource_lecturers, l_frequent_date, courses, time_of_cources
        )
        free_dates = [
            x for x in dates if x not in [item[0] for item in l_frequent_date]
        ]
        best_possible_date = [
            c
            for c in [item[0] for item in c_frequent_date]
            if c in free_dates + [item[0] for item in l_frequent_date]
        ]
        for lecturer in cource_lecturers:
            if (lecturer not in courses) and len(
                np.intersect1d(
                    np.array(best_possible_date),
                    np.array(get_only_dates_from_wishes(time_of_cources, lecturer)),
                )
            ):
                array_best_possible_date = [
                    item
                    for item in best_possible_date
                    if item in get_only_dates_from_wishes(time_of_cources, lecturer)
                ]
                courses[lecturer] = array_best_possible_date[0]

    dates_1 = sorted(
        [
            (item, list(courses.values()).count(item))
            for item in set(list(courses.values()))
        ],
        key=lambda t: t[1],
    )
    lecturer_possible_dates = [
        date[0] for date in dates_1 if date[1] > len(courses) * 3 / 10
    ]
    for lecturer_key in courses:
        if courses[lecturer_key] not in lecturer_possible_dates:
            # print(
            #     f"Преподавателя {get_lecturer_name(lecturer_key)} не получилось поставить в указанное им время, он может проводить экзамен в следующие даты:{lecturer_possible_dates}, по умолчанию поставлен на {lecturer_possible_dates[-1]}"
            # )
            courses[lecturer_key] = lecturer_possible_dates[-1]
    schedule = []
    for i in range(len(dates)):
        day = []
        if dates[i] in courses.values():
            for lecturer_key, date in courses.items():
                if date == dates[i]:
                    day.append(
                        DayOfSchedule(
                            group="all course",
                            lecturer=lecturer_key,
                            discipline=subjects[lecturer_key],
                        )
                    )
        if day:
            schedule.append(day)
        else:
            schedule.append("")
    schedule_dict = {gr: schedule.copy() for gr in groups}
    return schedule_dict
