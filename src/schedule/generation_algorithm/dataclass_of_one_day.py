from dataclasses import dataclass


@dataclass
class DayOfSchedule:
    group: str
    lecturer: str = ""
    discipline: str = ""
    time: str = ""
    auditorium: str = ""

    def __str__(self):
        if isinstance(self.discipline, list):
            return f"{self.discipline[0]}, {self.lecturer}, в {self.time}, ауд. {self.auditorium}"
        elif self.discipline != "":
            return f"{self.discipline}, {self.lecturer},\n в {self.time}, ауд. {self.auditorium}"
        return ""
