from schedule.generation_algorithm.get_data import get_lecturer_name


def get_time_to_one_dataclass(lecturers_wishes, date):
    for pare in lecturers_wishes:
        if date == pare[0]:
            return pare
    return "10:10"


def get_time_from_withes(schedule, time_of_lecturers, time_of_curses, period):
    for one_group in list(schedule.values()):
        for one_day, date in zip(one_group, period):
            if not isinstance(one_day, str) and not isinstance(one_day, list):
                time = get_time_to_one_dataclass(
                    time_of_lecturers[one_day.lecturer], date
                )
                if isinstance(time, str):
                    one_day.time = time
                else:
                    one_day.time = time[1]
                    time_of_lecturers[one_day.lecturer].remove(time)
            if isinstance(one_day, list):
                for one_dc in one_day:
                    this_lecturer = one_dc.lecturer
                    time = get_time_to_one_dataclass(
                        time_of_curses[this_lecturer], date
                    )
                    if isinstance(time, str):
                        one_dc.time = time
                    else:
                        one_dc.time = time[1]
                        time_of_curses[this_lecturer].remove(time)


def got_lecturers_name(schedule, period):
    for one_group in list(schedule.values()):
        for one_day, date in zip(one_group, period):
            if not isinstance(one_day, str) and not isinstance(one_day, list):
                one_day.lecturer = get_lecturer_name(one_day.lecturer)
            if isinstance(one_day, list):
                for one_dc in one_day:
                    if isinstance(one_dc.lecturer, int):
                        one_dc.lecturer = get_lecturer_name(one_dc.lecturer)
