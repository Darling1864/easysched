def exist_sub_in_schedule_group(period, subject):
    for day in period:
        if day and not isinstance(day, list):
            if subject == day.discipline:
                return True
    return False
