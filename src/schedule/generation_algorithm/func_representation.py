import datetime as dt

import pandas as pd


def period_of_session(start, end):
    start_date = dt.datetime(*start)
    end_date = dt.datetime(*end)

    return pd.date_range(min(start_date, end_date), max(start_date, end_date))


def copy_dictionary(schedule):
    return {x[0]: x[1].copy() for x in schedule.items()}


def reform_list_to_str(day):
    return ",\n".join([str(exam) for exam in day])


def sum_lists(local_list):
    total = []
    for l in local_list:
        if l:
            total += l
    return total


def get_only_dates_from_wishes(dict_of_wishes, lecturer):
    result = []
    if lecturer in dict_of_wishes:
        for dataset in dict_of_wishes[lecturer]:
            result.append(dataset[0])
    return result


def get_title_table():
    now = dt.date.today()
    p_year = (
        f"{now.year}-{now.year+1}" if now.month >= 8 else f"{now.year-1}-{now.year}"
    )
    n_session = 1 if (now.month >= 8 and now.month <= 3) else 2
    return f"Расписание экзаменов за {n_session} семестр {p_year} учебный год."
