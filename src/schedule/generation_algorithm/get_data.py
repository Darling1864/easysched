import django
import os
import datetime

from schedule.models import (
    Group,
    SubjectLectorGroup,
    Auditorium,
    Wishes,
    Lecturer,
    Subject,
)

# TODO в настройки вынести
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "session_schedule.settings")
django.setup()


def get_groups():
    all_groups = {f"{i}": [] for i in range(1, 5)}

    now = datetime.date.today()
    year = now.year if now.month >= 8 else now.year - 1

    for item in Group.objects.values():
        all_groups[f"{year - item['year_of_study'] + 1}"].append(
            f"{item['code_institute_id']}-{item['group_number']}"
        )

    return all_groups


# course = 'f' or 't'
def get_subjects(course):
    queryset = (
        SubjectLectorGroup.objects.select_related("subject")
        .filter(subject__is_elective_course=course)
        .select_related("lecture")
    )
    all_subjects = {f"{i}": {} for i in range(1, 5)}

    for con in queryset:
        lecturer = con.lecture.id
        subject = con.subject.subject_name
        course = f"{con.subject.course_number}"
        if lecturer not in all_subjects[course]:
            all_subjects[course][lecturer] = [subject]
        elif subject not in all_subjects[course][lecturer]:
            all_subjects[course][lecturer].append(subject)

    return all_subjects


def get_auditoriums():
    all_auditoriums = []

    for item in Auditorium.objects.values():
        all_auditoriums.append(
            f"{item['auditorium_number']}, {item['building_address']}"
        )

    return all_auditoriums


def get_lecturer_time(course):
    lecturer_obj = (
        SubjectLectorGroup.objects.select_related("lecture")
        .select_related("subject")
        .filter(subject__is_elective_course=course)
    )
    ids = {item["lecture_id"] for item in lecturer_obj.values()}
    queryset = Wishes.objects.filter(lecturer_id__in=ids).select_related("lecturer")
    time_of_lecturers = {}
    for con in queryset:
        date = con.start_time.date()
        start_time = con.start_time.time()
        lecturer = con.lecturer.id
        if lecturer not in time_of_lecturers:
            time_of_lecturers[lecturer] = [
                (date.strftime("%d.%m.%Y"), start_time.strftime("%H:%M"))
            ]
        else:
            time_of_lecturers[lecturer].append(
                (date.strftime("%d.%m.%Y"), start_time.strftime("%H:%M"))
            )
    return time_of_lecturers


def get_lecturer_name(lecturer_id):
    lecturer_obj = Lecturer.objects.get(id=lecturer_id)
    return f"{lecturer_obj.last_name} {lecturer_obj.first_name[0]}.{lecturer_obj.middle_name[0]}."


def get_streams(group_number):
    lecturer_obj = (
        SubjectLectorGroup.objects.select_related("lecture")
        .select_related("subject")
        .filter(group__group_number=group_number[3:])
        .filter(subject__is_elective_course=False)
    )
    x = {}
    for lecturer in lecturer_obj:
        if lecturer.lecture_id not in x:
            x[lecturer.lecture_id] = [lecturer.subject.subject_name]
        else:
            x[lecturer.lecture_id].append(lecturer.subject.subject_name)

    return x
