from schedule.generation_algorithm.func_representation import (
    get_only_dates_from_wishes,
)


def day_is_busy(schedule, day_index, sub):
    for group_days in list(schedule.values()):
        # TODO group_days[day_index] должен содержать none вместо пустых строк
        if not isinstance(group_days[day_index], list) and group_days[day_index] != "":
            if group_days[day_index].discipline == sub:
                return True
    return False


def free_days(schedule, group, day_of_exam):
    start = day_of_exam - 2 if day_of_exam >= 2 else 0
    finish = (
        day_of_exam + 3
        if day_of_exam <= len(schedule[group]) - 3
        else len(schedule[group])
    )
    for day in schedule[group][start:finish]:
        if day != "":
            return False
    return True


def is_lecturer_has_no_exams_that_day(schedule, day_index, that_lecturer):
    for group_days in list(schedule.values()):
        if not isinstance(group_days[day_index], list) and group_days[day_index] != "":
            if group_days[day_index].lecturer == that_lecturer:
                return False
        elif isinstance(group_days[day_index], list):
            dataclass = group_days[day_index][0]
            if dataclass.lecturer == that_lecturer:
                return False
    return True


def is_lecturer_has_two_exams_that_day(lecturer, day, time_of_lecturers):
    return get_only_dates_from_wishes(time_of_lecturers, lecturer).count(day) == 2


def lecturer_has_free_time_to_this_exam(schedule, day_index, that_lecturer):
    count_of_exams_one_day = 0
    list_of_elective_curses = []
    for group_days in list(schedule.values()):
        if not isinstance(group_days[day_index], list) and group_days[day_index] != "":
            if group_days[day_index].lecturer == that_lecturer:
                count_of_exams_one_day += 1
                if count_of_exams_one_day >= 2:
                    return False
        elif (
            isinstance(group_days[day_index], list)
            and group_days[day_index][0].discipline not in list_of_elective_curses
        ):
            list_of_elective_curses.append(group_days[day_index][0].discipline)
            dataclass = group_days[day_index][0]
            if dataclass.lecturer == that_lecturer:
                count_of_exams_one_day += 1
                if count_of_exams_one_day >= 2:
                    return False
    return True
