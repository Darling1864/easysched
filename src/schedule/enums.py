from django.db import models


class TypeEducation(models.TextChoices):
    bachelor = "Бакалавриат", "Бакалавриат"
    magistracy = "Магистратура", "Магистратура"
    graduate_school = "Аспирантура", "Аспирантура"
    specialty = "Специалитет", "Специалитет"


class TypeOfSubject(models.IntegerChoices):
    ELECTIVE_COURSE = 1, "Курс по выбору"
    COMMON_SUBJECT = 0, "Общий предмет"
