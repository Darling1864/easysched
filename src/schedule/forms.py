from django import forms
from datetime import datetime
from django.core.exceptions import ValidationError

from schedule.models import Subject, Lecturer, SubjectLectorGroup, Wishes, Group


class AuthForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())


class SubjectForm(forms.ModelForm):
    class Meta:
        model = Subject
        fields = [
            "subject_name",
            "course_number",
            "type_of_education",
            "is_elective_course",
            "type_of_education",
        ]
        labels = {
            "subject_name": "Название дисциплины: ",
            "course_number": "Номер курса: ",
            "type_of_education": "Вид образования: ",
            "is_elective_course": "Тип курса: ",
        }

    def __init__(self, *args, **kwargs):
        super(SubjectForm, self).__init__(*args, **kwargs)
        self.fields["subject_name"].required = True
        self.fields["course_number"].required = True
        self.fields["type_of_education"].required = True
        self.fields["is_elective_course"].required = True


class SubjectDetailForm(forms.ModelForm):
    def save(self, *args, **kwargs):
        subject = self.initial["subject"]
        self.instance.note = subject
        instance = super(SubjectDetailForm, self).save(*args, **kwargs)
        return instance

    class Meta:
        model = Subject
        fields = (
            "subject_name",
            "course_number",
            "type_of_education",
            "is_elective_course",
        )


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = [
            "code_institute",
            "group_number",
            "year_of_study",
            "leader_tg",
            "type_of_education",
        ]
        labels = {
            "code_institute": "Институт: ",
            "group_number": "Номер группы: ",
            "year_of_study": "Год поступления: ",
            "leader_tg": "Телеграмм старосты: ",
            "type_of_education": "Вид образования: ",
        }

    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)
        self.fields["code_institute"].required = True
        self.fields["group_number"].required = True
        self.fields["year_of_study"].required = True
        self.fields["type_of_education"].required = True

    def clean_year_of_study(self):
        year_of_study = self.cleaned_data["year_of_study"]
        group_number = self.cleaned_data["group_number"]
        max_year = datetime.now().year
        min_year = datetime.now().year - 4

        if year_of_study < min_year or year_of_study > max_year:
            raise ValidationError("Некорректный год поступления")

        if Group.objects.filter(group_number=group_number).exists():
            raise ValidationError("Группа с указанным номером уже существует.")

        return year_of_study


class GroupDetailForm(forms.ModelForm):
    def save(self, *args, **kwargs):
        group = self.initial["group"]
        self.instance.note = group
        instance = super(GroupDetailForm, self).save(*args, **kwargs)
        return instance

    class Meta:
        model = Group
        fields = [
            "code_institute",
            "group_number",
            "year_of_study",
            "leader_tg",
            "type_of_education",
        ]


class LecturerForm(forms.ModelForm):
    class Meta:
        model = Lecturer
        fields = [
            "email",
            "last_name",
            "first_name",
            "middle_name",
        ]

    def save(self, *args, **kwargs):
        return super(LecturerForm, self).save(*args, **kwargs)


class LecturerDetailForm(forms.ModelForm):
    def save(self, *args, **kwargs):
        group = self.initial["lecturer"]
        self.instance.note = group
        instance = super(LecturerDetailForm, self).save(*args, **kwargs)
        return instance

    class Meta:
        model = Lecturer
        fields = (
            "email",
            "last_name",
            "first_name",
            "middle_name",
        )


class ConnectionForm(forms.ModelForm):
    class Meta:
        model = SubjectLectorGroup
        fields = ["lecture", "subject", "group"]
        labels = {"lecture": "Лектор: ", "subject": "Дисциплина: ", "group": "Группа: "}


class ScheduleFormOne(forms.ModelForm):
    class Meta:
        model = Lecturer
        fields = [
            "last_name",
            "first_name",
            "middle_name",
        ]
        widgets = {
            "first_name": forms.TextInput(
                attrs={
                    "id": "name",
                    "class": "edit",
                    "name": "name",
                }
            ),
            "middle_name": forms.TextInput(
                attrs={
                    "id": "surname",
                    "class": "edit",
                    "name": "surname",
                }
            ),
            "last_name": forms.TextInput(
                attrs={
                    "id": "patronymic",
                    "class": "edit",
                    "name": "patronymic",
                }
            ),
        }


class ScheduleFormTwo(forms.ModelForm):
    class Meta:
        model = Wishes
        fields = ["exam_duration", "wishes"]
        widgets = {
            "exam_duration": forms.TextInput(attrs={"type": "time"}),
            "wishes": forms.Textarea(attrs={"class": "wishes"}),
        }


class WishFormOne(forms.ModelForm):
    # create meta class
    class Meta:
        # specify model to be used
        model = Subject

        # specify fields to be used
        fields = [
            "subject_name",
            "course_number",
        ]


class WishFormTwo(forms.ModelForm):
    # create meta class
    class Meta:
        # specify model to be used
        model = Lecturer

        # specify fields to be used
        fields = [
            "last_name",
            "first_name",
            "middle_name",
        ]


class WishFormThree(forms.ModelForm):
    # create meta class
    class Meta:
        # specify model to be used
        model = Wishes

        # specify fields to be used
        fields = [
            "wishes",
        ]


class WishChange(forms.ModelForm):
    class Meta:
        model = Wishes

        fields = [
            "start_time",
            "exam_duration",
            "number_of_exams",
            "wishes",
        ]
