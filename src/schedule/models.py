from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import UserManager as DjangoUserManager
from django.db import models

from schedule.enums import TypeEducation, TypeOfSubject


class BaseModel(models.Model):
    class Meta:
        abstract = True


class UserManager(DjangoUserManager):
    def _create_user(self, email, password, commit=True, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        if commit:
            user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(BaseModel, AbstractBaseUser, PermissionsMixin):
    objects = UserManager()
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=255, null=True, blank=True)

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []


class Lecturer(models.Model):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    middle_name = models.CharField(max_length=255)
    tg = models.CharField(max_length=255, null=True, blank=True)
    phone_number = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return f"{self.last_name} {self.first_name} {self.middle_name}"


class Wishes(models.Model):
    lecturer = models.ForeignKey(Lecturer, on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    exam_duration = models.DurationField()
    number_of_exams = models.IntegerField()
    wishes = models.TextField(blank=True)


class Subject(models.Model):
    subject_name = models.CharField(max_length=255)
    course_number = models.IntegerField()
    type_of_education = models.CharField(
        max_length=15, choices=TypeEducation.choices, default="Бакалавриат"
    )
    is_elective_course = models.IntegerField(
        choices=TypeOfSubject.choices, default=TypeOfSubject.COMMON_SUBJECT
    )

    def __str__(self):
        return f"{self.subject_name}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["subject_name", "course_number"], name="constraint_subject"
            )
        ]


class Institute(models.Model):
    code_institute = models.IntegerField(primary_key=True, unique=True)
    institute_name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return f"{self.code_institute} {self.institute_name}"


class Group(models.Model):
    code_institute = models.ForeignKey(Institute, on_delete=models.CASCADE)
    group_number = models.CharField(max_length=10)
    year_of_study = models.IntegerField()
    leader_tg = models.CharField(max_length=255, null=True, blank=True)
    type_of_education = models.CharField(
        max_length=15, choices=TypeEducation.choices, default="Бакалавриат"
    )

    def __str__(self):
        return f"{self.group_number}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["code_institute", "group_number"], name="constraint_group"
            )
        ]


class SubjectLectorGroup(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    lecture = models.ForeignKey(Lecturer, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["lecture", "subject", "group"], name="constraint_unique"
            )
        ]


class Auditorium(models.Model):
    auditorium_number = models.CharField(max_length=255)
    building_address = models.CharField(max_length=255)


class Timetable(models.Model):
    exam_year = models.CharField(max_length=255)
    exam_season = models.CharField(max_length=255)
    link = models.URLField(max_length=255)


class Completion(models.Model):
    lecturer = models.ForeignKey(Lecturer, on_delete=models.CASCADE)
    is_fill = models.BooleanField()
