"""session_schedule URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from schedule.views.lecturer import (
    LecturerDetailView,
    LecturersListView,
    LecturerDeleteView,
    lecturer_form,
    LecturerCreateView,
    LecturerUpdateView,
)
from schedule.views.subject import (
    SubjectsListView,
    SubjectCreateFormView,
    SubjectUpdateView,
    SubjectDetailView,
    SubjectDeleteView,
)
from schedule.views.group import (
    GroupsListView,
    GroupUpdateView,
    GroupCreateFormView,
    GroupDetailView,
    GroupDeleteView,
)
from schedule.views.instruction import (
    send_letters,
    form,
    instruction_view,
    schedule_generation_view,
    list_wishes_view,
    form_edit,
)
from schedule.views.main import login_view, logout_view, main


urlpatterns = [
    path("", main, name="main"),
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("instruction/", instruction_view, name="instruction"),
    path("instruction/generate/", schedule_generation_view, name="generate"),
    path("instruction/wishes/", list_wishes_view, name="wishes"),
    path("instruction/wishes/<int:id>/", lecturer_form, name="lecturer_form"),
    path("form/", form, name="form"),
    path("form/<int:id>", form_edit, name="form_edit"),
    path("instruction/send_letters/", send_letters, name="send_letters"),
    path("lecturers/", LecturersListView.as_view(), name="lecturers"),
    path("lecturers/add_lecturer", LecturerCreateView.as_view(), name="add_lecturer"),
    path("lecturers/<int:id>/", LecturerDetailView.as_view(), name="profile"),
    path("lecturers/<int:id>/edit", LecturerUpdateView.as_view(), name="edit_lecturer"),
    path(
        "lecturers/<int:id>/delete",
        LecturerDeleteView.as_view(),
        name="delete_lecturer",
    ),
    path("subjects/", SubjectsListView.as_view(), name="subjects"),
    path("subjects/add_subjects", SubjectCreateFormView.as_view(), name="add_subject"),
    path("subjects/<str:subject_name>", SubjectDetailView.as_view(), name="subject"),
    path(
        "subjects/<str:subject_name>/edit",
        SubjectUpdateView.as_view(),
        name="edit_subject",
    ),
    path(
        "subjects/<str:subject_name>/delete",
        SubjectDeleteView.as_view(),
        name="delete_subject",
    ),
    path("groups/", GroupsListView.as_view(), name="groups"),
    path("groups/add_group", GroupCreateFormView.as_view(), name="add_group"),
    path("groups/<str:group_number>", GroupDetailView.as_view(), name="group"),
    path(
        "groups/<str:group_number>/edit", GroupUpdateView.as_view(), name="edit_group"
    ),
    path(
        "groups/<str:group_number>/delete",
        GroupDeleteView.as_view(),
        name="delete_group",
    ),
]
